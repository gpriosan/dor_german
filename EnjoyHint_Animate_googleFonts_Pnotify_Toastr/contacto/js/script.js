 $(document).on('click','#enviar',function (evt) {
    evt.preventDefault();
    toastr.success("Fantástico!!!");
})


 $(function() {

    function Toast(type, css, msg) {
        this.type = type;
        this.css = css;
        this.msg = '' + msg + '';
    }

    var toasts = [
    new Toast('error', 'toast-top-full-width', 'Quieres más span...que fallo'),
    new Toast('info', 'toast-top-full-width', 'Te llenaremos de span tu correo'),
    new Toast('warning', 'toast-top-left', 'Cuidado! Mucho spam!'),
    new Toast('success', 'toast-top-right', 'Que de spam te vas a llevar...'),
    ];

    toastr.options.positionClass = 'toast-top-center';
    toastr.options.extendedTimeOut = 50000; //1000;
    toastr.options.timeOut = 10000;
    toastr.options.fadeOut = 2500;
    toastr.options.fadeIn = 250;

    var i = 0;

    $('#enviar').click(function () {
        $('#enviar').prop('disabled', true);
        delayToasts();
    });

    function delayToasts() {
        if (i === toasts.length) { return; }
        var delay = i === 0 ? 0 : 2100;
        window.setTimeout(function () { showToast(); }, delay);

        // re-enable the button        
        if (i === toasts.length-1) {
            window.setTimeout(function () {
                $('#enviar').prop('disabled', false);
                i = 0;
            }, delay + 1000);
        }
    }

    function showToast() {
        var t = toasts[i];
        toastr.options.positionClass = t.css;
        toastr[t.type](t.msg);
        i++;
        delayToasts();
    }
})
